package com.example.login;


import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UserDAO {

    private static final String INSERT_QUERY = "INSERT INTO registrierung ( `name`, `email`, `passwort`) VALUES ( ?, ?, ?)";

    public User get(long id){return null;}



    public void insertRecord(User user) {


        Connection connection = DBConnection.getConnection();
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(
                    user.getPassword().getBytes(StandardCharsets.UTF_8));
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.setString(3, encodedhash.toString());


            System.out.println(preparedStatement);

            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void saveRecord(User user){

    }

    public void updateRecord(User user){

    }

    public void deleteRecord(User user) {

    }


}
